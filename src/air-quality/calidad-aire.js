import { resultsetWithFields, buildTableCalidadAire } from 'vlcishared';
import { getBaseURI } from '../utils/env-utils';

export function tableCalidadAire() {
  $(document).ready(() => {
    $.ajax({
      type: 'GET',
      url: `${getBaseURI()}calidadaire.cors`,
      dataType: 'text',
      success(json) {
        const data = JSON.parse(json);
        const array = resultsetWithFields(data);
        const trElements = buildTableCalidadAire(array);
        trElements.forEach((tr) => {
          const tbody = document.querySelector('#tabla_dinamica > tbody');
          tbody.appendChild(tr);
        });
      },
    });
  });
}
