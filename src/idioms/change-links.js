export default function changeLangLinks() {
  const linkLangEs = document.getElementById('link-lang-es');
  const linkLangCa = document.getElementById('link-lang-ca');
  const linkLangEn = document.getElementById('link-lang-en');

  const plid = themeDisplay.getPlid();

  linkLangEs.setAttribute(
    'href',
    `/c/portal/update_language?p_l_id=${plid}&redirect=%2Fweb%2Fvalenciaalminut&languageId=es_ES`,
  );
  linkLangCa.setAttribute(
    'href',
    `/c/portal/update_language?p_l_id=${plid}&redirect=%2Fweb%2Fvalenciaalminut&languageId=ca_ES`,
  );
  linkLangEn.setAttribute(
    'href',
    `/c/portal/update_language?p_l_id=${plid}&redirect=%2Fweb%2Fvalenciaalminut&languageId=en_ES`,
  );
}
