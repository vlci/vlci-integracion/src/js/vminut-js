export default function formatNumberLanguage(value) {
  // Obtiene el idioma del navegador
  const usrlang = navigator.language || navigator.userLanguage;

  return usrlang === 'es-ES'
    ? parseInt(value, 10).toLocaleString('de-DE', { minimumFractionDigits: 0 })
    : parseInt(value, 10).toLocaleString(usrlang, { minimumFractionDigits: 0 });
}
