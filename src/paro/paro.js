import TEXT_PARO from './textos';
import { getBaseURI } from '../utils/env-utils';
import graficaDatosParo from '../charts/grafica-paro';

const IDIOMA = Liferay.ThemeDisplay.getLanguageId();

/**
 * Formateo de fecha de un trimestre
 * @param {*} date YYYY-MM
 * @returns trimestre formato MM'T' YYYY
 */
const formatTrimestre = (date) => {
  const [anyo, mes] = date.split('-');

  return `${parseFloat(mes) / 3}T ${anyo}`;
};

/**
 * Devuelve un String con el numero con separacion por punto entre miles
 * @param {*} number
 * @returns
 */
const formatNumberParo = (number) => number.toLocaleString('es-ES', { minimumFractionDigits: 0 });

/**
 * Obtener la suma del paro femenino más masculino.
 * @param {*} data
 * @returns array data
 */
const getDataTotalValue = (data) => {
  const dataParoTotal = [];
  data.forEach((element, index, array) => {
    if (index % 2 === 0 && index + 1 < array.length) {
      dataParoTotal.push({
        kpivalue: parseFloat(element.kpivalue) + parseFloat(array[index + 1].kpivalue),
        calculationperiod: element.calculationperiod,
      });
    }
  });
  return dataParoTotal;
};

/**
 * Devuelve los datos con la tendencia respecto al mes anterior.
 * @param {*} data
 * @returns array de objetos
 */
const getDataGeneroTendencia = (data) => {
  const dataGeneroTendencia = [];
  let tendencia = 'mateix';

  data.forEach((element, index, array) => {
    if (index > 1) {
      if (parseFloat(element.kpivalue) > parseFloat(array[index % 2].kpivalue)) {
        tendencia = 'puja';
      } else if (parseFloat(element.kpivalue) < parseFloat(array[index % 2].kpivalue)) {
        tendencia = 'baixa';
      }
      dataGeneroTendencia.push({ tendencia, ...element });
    }
  });

  return dataGeneroTendencia;
};

/**
 * Devuelve los datos con la tendencia respecto al mes anterior.
 * @param {*} data
 * @returns array de objetos
 */
const getDataWithTendencia = (data) => {
  const dataTrimestral = [];

  data.forEach((element, index, array) => {
    if (index > 0) {
      let tendencia = 'mateix';
      if (parseFloat(element.kpivalue) > parseFloat(array[index - 1].kpivalue)) {
        tendencia = 'puja';
      } else if (parseFloat(element.kpivalue) < parseFloat(array[index - 1].kpivalue)) {
        tendencia = 'baixa';
      }

      dataTrimestral.push({ tendencia, ...element });
    }
  });

  return dataTrimestral;
};

/**
 * Agregar a los elementos del DOM los datos Trimestrales del paro
 * @param {*} data
 */
const setDataTrimestralDOM = (data) => {
  const container = document.querySelector('.atur-trimestral');
  let containerElement;

  data.forEach((element) => {
    containerElement = document.createElement('p');
    containerElement.classList.add(element.tendencia);
    containerElement.innerHTML = `<span>${TEXT_PARO[IDIOMA].get('Tasa')} ${formatTrimestre(
      element.calculationperiod,
    )}</span> <span> ${element.kpivalue.replace('.', ',')}% </span>`;
    container.appendChild(containerElement);
  });
};

/**
 * Agregar a los elementos del DOM los datos Mensuales totales del paro
 * @param {*} data
 */
const setDataMensualTotalDOM = (data) => {
  const containerParoAnterior = document.querySelector('.atur-darrer-trimestre');

  const dataParoAnterior = data.slice(0, 3);
  const dataParoActual = data.slice(3);

  let containerElement;

  dataParoAnterior.forEach((element) => {
    containerElement = document.createElement('p');
    containerElement.classList.add(element.tendencia);

    containerElement.innerHTML = `<span>${TEXT_PARO[IDIOMA].get(
      element.calculationperiod.substring(5),
    )} ${element.calculationperiod.substring(0, 4)}</span><span>${formatNumberParo(
      parseFloat(element.kpivalue),
    )}</span>`;
    containerParoAnterior.appendChild(containerElement);
  });

  const containerActual = document.querySelector('#actual');
  containerActual.classList.add(dataParoActual[0].tendencia);

  containerActual.innerHTML = ` <span class="curr-month-span"> ${TEXT_PARO[IDIOMA].get(
    dataParoActual[0].calculationperiod.substring(5),
  )} ${dataParoActual[0].calculationperiod.substring(
    0,
    4,
  )}</span><span class="curr-month-span"> ${formatNumberParo(
    parseFloat(dataParoActual[0].kpivalue),
  )} </span>`;
};

/**
 * Agregar a los elementos del DOM los datos Mensual de Paro masculino y femenino
 * @param {*} data
 */
const setDataMensualGeneroDOM = (data) => {
  const containerParoFemenino = document.querySelector('#actualF');
  const containerParoMasculino = document.querySelector('#actualM');

  data.forEach((element) => {
    const containerElementTitulo = document.createElement('span');
    const containerElementParo = document.createElement('span');
    containerElementParo.className = 'curr-month-span';

    containerElementTitulo.innerHTML = `${TEXT_PARO[IDIOMA].get('Paro')} ${
      element.sliceanddicevalue
    } ${TEXT_PARO[IDIOMA].get(
      element.calculationperiod.substring(5),
    )} ${element.calculationperiod.substring(0, 4)}`;

    containerElementParo.innerHTML = ` ${formatNumberParo(parseFloat(element.kpivalue))}`;

    if (element.sliceanddicevalue === TEXT_PARO[IDIOMA].get('GeneroF')) {
      containerParoFemenino.classList.add(element.tendencia);
      containerParoFemenino.appendChild(containerElementTitulo);
      containerParoFemenino.appendChild(containerElementParo);
    } else if (element.sliceanddicevalue === TEXT_PARO[IDIOMA].get('GeneroM')) {
      containerParoMasculino.classList.add(element.tendencia);
      containerParoMasculino.appendChild(containerElementTitulo);
      containerParoMasculino.appendChild(containerElementParo);
    }
  });
};

/**
 * Compara los datos de genero primero por año, luego por mes y por ultimo por genero
 * @param {*} entryA
 * @param {*} entryB
 * @returns
 */
const sortDataByDateAndGender = (entryA, entryB) => {
  const [entryAYear, entryAMonth] = entryA.calculationperiod.split('-');
  const [entryBYear, entryBMonth] = entryB.calculationperiod.split('-');

  if (entryAYear > entryBYear) return 1;
  if (entryAYear < entryBYear) return -1;
  if (entryAMonth > entryBMonth) return 1;
  if (entryAMonth < entryBMonth) return -1;
  if (entryA.sliceanddicevalue > entryB.sliceanddicevalue) return 1;
  if (entryA.sliceanddicevalue < entryB.sliceanddicevalue) return -1;

  return 0;
};

export async function getDataTrimestralParo() {
  try {
    const response = await fetch(`${getBaseURI()}paroTrimestral.cors?limit=5`);
    const data = await response.json();

    setDataTrimestralDOM(getDataWithTendencia(data.reverse()));
  } catch (error) {
    console.error(error);
  }
}

export async function getDataMensualParo() {
  try {
    const response = await fetch(`${getBaseURI()}paroMensual.cors`);
    const data = await response.json();
    data.sort(sortDataByDateAndGender);

    const dataTotal = getDataTotalValue(data);
    const dataTendencia = getDataWithTendencia(dataTotal.slice(-5));

    setDataMensualTotalDOM(dataTendencia);
    setDataMensualGeneroDOM(getDataGeneroTendencia(data.slice(-4)));

    graficaDatosParo(data, dataTotal);
  } catch (error) {
    console.error(error);
  }
}
