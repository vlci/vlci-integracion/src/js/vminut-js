import { envUtil } from './utils/env-utils';
import { printCamarasTrafico } from './cameras/camaras-trafico';
import { googleAnalyticsReport, googleTagManagerInner } from './google-analitics/google-analitics';
import loadCyclingInformationData from './bicycles/valenbisi';
import graficas from './charts/graficas-generales';
import getDataPublicParkings from './parkings/publicparkings';
import getDataSmartParkings from './parkings/smartparking';
import { tableCalidadAire } from './air-quality/calidad-aire';
import { loadIframe, changeIframe } from './maps/geoportal';
import changeLangLinks from './idioms/change-links';
import cuentabicis from './bicycles/cuentabicis';
import { getDataTrimestralParo, getDataMensualParo } from './paro/paro';
/** ************************************************************************************* */
/* EJECUCIÓN DE FUNCIONES DESPUES DE QUE CARGUE LA PAGINA */
/** ************************************************************************************ */

graficas();
loadCyclingInformationData();
printCamarasTrafico();
tableCalidadAire();
loadIframe('iframe-transit');
changeLangLinks();
cuentabicis();
getDataPublicParkings();
getDataSmartParkings();
getDataTrimestralParo();
getDataMensualParo();

document.querySelector('#menu-mapes').addEventListener('click', changeIframe);

// Funciones Analiticas que se ejecutan solo en PRO
if (envUtil.isProduction()) {
  googleAnalyticsReport();
  googleTagManagerInner();
}
