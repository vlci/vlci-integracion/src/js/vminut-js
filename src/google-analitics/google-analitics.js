/* Función que se usa en el caso de que no funcione Google Analytics
a través de la configuración de Liferay.
De todas formas es necesario especificar el código de seguimiento en la configuración de la pagina
(Configuración/Avanzado/Estadísticas/Identificador de Google Analytics) */
/** ************************************************************************************ */
export function googleAnalyticsReport() {
  const dataLayer = window.dataLayer || [];
  function gtag() {
    dataLayer.push(...args);
  }
  gtag('js', new Date());
  // Codigo Analitics de Pre
  // gtag('config', 'UA-203691838-1');
  gtag('config', 'UA-98261417-1');
}

/** ************************************************************************************* */
/* GOOGLE TAG MANAGER */
/* Esta función inyecta el codigo javascript que necesita Google Tag Manager en el
Head de la pagina */
/** ************************************************************************************ */
export function googleTagManagerInner() {
  const head = document.getElementsByTagName('head')[0];
  const scriptNode = document.createElement('script');
  scriptNode.type = 'text/javascript';
  const stringHTML = `
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NX9M7NF');
    `;
  scriptNode.innerHTML = stringHTML;
  head.insertBefore(scriptNode, head.firstChild);
}
