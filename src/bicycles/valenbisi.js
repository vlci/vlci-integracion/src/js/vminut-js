import { envUtil } from '../utils/env-utils';

export default function loadCyclingInformationData() {
  $.ajax({
    type: 'GET',
    url: `${envUtil.getValenbisiURL()}query?where=1%3D1&geometryType=esriGeometryEnvelope&spatialRel=esriSpatialRelIntersects&units=esriSRUnit_Foot&outFields=updated_at%2Cnumber+%2Cavailable%2Cfree%2Ctotal%2Cgid&returnGeometry=false&returnTrueCurves=false&returnIdsOnly=false&returnCountOnly=false&returnZ=false&returnM=false&returnDistinctValues=false&returnExtentOnly=false&featureEncoding=esriDefault&f=pjson`,
    async: true,
    dataType: 'json',
    success(data) {
      const response = $(data);
      const total = response[0].features.length;
      let fullStation = 0;
      let emptyStation = 0;
      let available = 0;
      let cycleInUse = 0;
      const totalBicis = 2750;
      $.each(response[0].features, function () {
        if (this.attributes.free === this.attributes.total) {
          fullStation += 1;
        }
        if (this.attributes.available === 0) {
          emptyStation += 1;
        }
        available += parseInt(this.attributes.available, 10);
        cycleInUse = totalBicis - available;
      });
      fullStation = ((fullStation / total) * 100).toFixed(1);
      emptyStation = ((emptyStation / total) * 100).toFixed(1);
      fullStation = fullStation.replace('.', ',');
      emptyStation = emptyStation.replace('.', ',');
      $('#fullStation').text(`${fullStation}%`);
      $('#availableCycle').text(available);
      $('#emptyStation').text(`${emptyStation}%`);
      $('#freeCycle').text(cycleInUse);
    },
  });
}
