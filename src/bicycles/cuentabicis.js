import { getBaseURI } from '../utils/env-utils';
import dibujarGauge from '../charts/graficas-gauge';
import formatNumberLanguage from '../idioms/format-languages';

// Objeto contenedor de los datos
const language = Liferay.ThemeDisplay.getLanguageId();
const urlCors = `${getBaseURI()}countbicis.cors?lang=${language}`;
const Datas = [
  {
    url: urlCors,
    DOMElementId: 'value_blanquerias_dia',
    entityId: 'intensidad_bicis_daily_f32',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'value_blanquerias_mes',
    entityId: 'intensidad_bicis_monthly_f32',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'value_blanquerias_anyo',
    entityId: 'intensidad_bicis_annual_f32',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'grafica_blanquerias_rt',
    entityId: 'intensidad_bicis_rt_f32',
    description: '',
    value: '',
    valueMax: '800',
  },
  {
    url: urlCors,
    DOMElementId: 'value_guillem_dia',
    entityId: 'intensidad_bicis_daily_f33',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'value_guillem_mes',
    entityId: 'intensidad_bicis_monthly_f33',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'value_guillem_anyo',
    entityId: 'intensidad_bicis_annual_f33',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'grafica_guillem_rt',
    entityId: 'intensidad_bicis_rt_f33',
    description: '',
    value: '',
    valueMax: '700',
  },
  {
    url: urlCors,
    DOMElementId: 'value_colom_dia',
    entityId: 'intensidad_bicis_daily_f6',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'value_colom_mes',
    entityId: 'intensidad_bicis_monthly_f6',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'value_colom_anyo',
    entityId: 'intensidad_bicis_annual_f6',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'grafica_colom_rt',
    entityId: 'intensidad_bicis_rt_f6',
    description: '',
    value: '',
    valueMax: '600',
  },
  {
    url: urlCors,
    DOMElementId: 'value_xativa_dia',
    entityId: 'intensidad_bicis_daily_f34',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'value_xativa_mes',
    entityId: 'intensidad_bicis_monthly_f34',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'value_xativa_anyo',
    entityId: 'intensidad_bicis_annual_f34',
    description: '',
    value: '',
    valueMax: '',
  },
  {
    url: urlCors,
    DOMElementId: 'grafica_xativa_rt',
    entityId: 'intensidad_bicis_rt_f34',
    description: '',
    value: '',
    valueMax: '900',
  },
];

// Almacena cada valor obtenido del respondeBody en su posición correspondiente del Data
const setDataFromResponseBody = (responseBody, index) => {
  const item = Datas[index];
  const { entityId } = item;
  const match = responseBody.find((responseItem) => responseItem.entityid === entityId);
  if (match) {
    item.description = match.description;
    item.value = formatNumberLanguage(match.value);
  }
};

/*
 * Esta función pinte en el HTML cada uno de los valores requeridos
 */
function mostrarValores() {
  const domElements = {};
  for (let i = 0, len = Datas.length; i < len; i += 1) {
    const id = Datas[i].DOMElementId;
    if (!domElements[id]) {
      domElements[id] = document.getElementById(id);
      // Pintamos los valores de dia, mes anyo y dijamos gráfica para los realtime
      if (!id.includes('grafica_')) {
        domElements[id].innerHTML = Datas[i].value;
      } else {
        dibujarGauge(id, Datas[i].value, Datas[i].valueMax);
      }
    }
  }
}

/*
 *  Funcion para obtener los datos de la query y pintarlas en HTML
 */
export default async function cuentabicis() {
  await Promise.all(
    Datas.map(async (req, index) => {
      const response = await fetch(`${req.url}`);
      const data = await response.json();
      setDataFromResponseBody(data, index);
    }),
  );
  mostrarValores();
}
