import { getBaseURI } from '../utils/env-utils';

const ventanaInformacion = document.querySelector('#aparcaments');
const PARKINGS_ON_DIV = 6;
const CAROUSEL_INTERVAL = 8000;

/**
 * Crea una ul que actuará como slider en el carusel.
 * @param {Number} index
 * @returns un elemento ul con un id especial para usarlo en el carusel.
 */
function createSliderElement(index) {
  const listElement = document.createElement('ul');
  listElement.classList.add('carousel-item');
  listElement.classList.add('grafica');
  listElement.setAttribute('id', `parkingLista${index}`);
  if (index === 0) {
    listElement.classList.add('active');
  }
  return listElement;
}

/**
 * Crea un elemento li para navegar por los sliders.
 * @param {Number} index
 * @returns un elemento li con los otros elementos deseados adentro.
 */
function createNavigationPoint(index) {
  const carouselIndicator = document.createElement('li');
  if (index === 0) {
    carouselIndicator.classList.add('active');
  }
  carouselIndicator.setAttribute('data-slide-to', String(index));
  carouselIndicator.setAttribute('data-target', '#carouselParkings');

  const navButton = document.createElement('button');
  navButton.setAttribute('aria-current', 'true');
  navButton.setAttribute('tabindex', '-1');
  navButton.classList.add('current');

  const spanHidden = document.createElement('span');
  spanHidden.setAttribute('aria-hidden', 'true');
  spanHidden.classList.add('ocultovisual');
  spanHidden.innerText = String(index + 1);

  navButton.appendChild(spanHidden);
  carouselIndicator.appendChild(navButton);

  return carouselIndicator;
}

/**
 * Crea una ul que actuará como slider en el carusel.
 * @param {Number} index
 * @returns un elemento ul con un id especial para usarlo en el carusel.
 */
function createListElement(element, index) {
  const li = document.createElement('li');

  li.classList.add('buit');

  li.id = `list${index}`;
  li.innerHTML = `<span id="record${index}">&nbsp;</span><span id="value${index}">&nbsp;</span>`;
  li.addEventListener('click', (event) => {
    const parkingsContentDiv = document.querySelector('#parkingsInner');
    const graficoAnterior = document.querySelector('[id^="chart"]');
    const graficoPadre = event.target.parentNode.parentNode;

    // Control de la visualización de los graficos
    // Si existe un grafico anterior lo elimina. Si el grafico es el mismo no se hace nada sino se
    // dibuja el nuevo. Cuando no hay anterior se dibuja el nuevo.
    if (graficoAnterior != null) {
      graficoAnterior.remove();
    }
    graficoPadre.classList.add('grafica');
    parkingsContentDiv.classList.add('grafica');
  });

  return li;
}

/**
 * Crea los elementos HTML de la lista de parkings
 * @param {Array} result
 */
function loadRowsOfParking(result) {
  const parkingsContentDiv = document.querySelector('#parkingsInner');
  const carouselIndicatorsList = document.querySelector('.carousel-indicators');

  for (let i = 0; i < result.length; i += PARKINGS_ON_DIV) {
    const listElement = createSliderElement(i / PARKINGS_ON_DIV);

    const chunk = result.slice(i, i + PARKINGS_ON_DIV);
    chunk.forEach((element, index) => {
      const indexInResult = i + index;
      listElement.appendChild(createListElement(element, indexInResult));
    });
    parkingsContentDiv.append(listElement);

    carouselIndicatorsList.appendChild(createNavigationPoint(i / PARKINGS_ON_DIV));
  }
}

/**
 * Rellena con información la lista de parkings y le agrega estilos
 * @param {Array} result
 */
function processDataParking(result) {
  result.forEach((element, index) => {
    const plazasLibres = parseInt(element.availablespotnumber, 10);
    const nombreParking = element.name;
    const elementList = document.querySelector(`#list${index}`);
    const elementRecord = document.querySelector(`#record${index}`);
    const elementValue = document.querySelector(`#value${index}`);

    elementValue.textContent = plazasLibres;
    elementRecord.textContent = nombreParking;

    if (element.operationalstatus === 'ok' && plazasLibres > 0 && plazasLibres < 10) {
      elementList.classList.remove('buit');
    } else if (element.operationalstatus === 'ok' && plazasLibres === 0) {
      elementList.classList.remove('buit');
      elementList.classList.add('ple');
    } else if (element.operationalstatus !== 'ok') {
      elementList.classList.remove('buit');
      elementList.classList.add('nd');
    }
    elementRecord.textContent = nombreParking;
    elementValue.textContent = element.operationalstatus !== 'ok' ? '--' : plazasLibres;
  });
}

/**
 * Obtiene la informacion de los parkings a mostrar.
 * @returns array de objetos con la informacion de parkings.
 */
export default async function getDataPublicParkings() {
  try {
    const responseParkings = await fetch(`${getBaseURI()}publicparkings.cors`);
    const dataParkings = await responseParkings.json();

    loadRowsOfParking(dataParkings);
    processDataParking(dataParkings);
  } catch (error) {
    console.log(error);
  }
}

// EVENTOS
document.querySelector('#btnInformacion').addEventListener('click', () => {
  ventanaInformacion.style.display = '';
});

document.querySelector('#aparcaments .tancar').addEventListener('click', () => {
  ventanaInformacion.style.display = 'none';
});

// Este trozo de código setea el intervalo de cambio de slide en el carusel, y luego añade el
// event listener al boton de start/stop, tiene que usar jquery
// porque bootstrap implemento las funciones en jquery

$('.carousel').carousel({
  interval: CAROUSEL_INTERVAL,
});
$('#carouselParkings').carousel('cycle');

const buttonStartStop = document.querySelector('#startStop');

buttonStartStop.addEventListener('click', () => {
  if (buttonStartStop.classList.contains('btn-play-paused')) {
    $('#carouselParkings').carousel('pause');
  } else {
    $('#carouselParkings').carousel('cycle');
  }
  $('.btn-play').toggleClass('btn-play-paused');
});
