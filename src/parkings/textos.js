const TEXT_PUBLIC_PARKINGS = {
  es_ES: new Map([
    ['leyendaPlazasLibres', 'Plazas libres'],
    ['tituloMasPlazas', 'Menos concurrido de lo habitual'],
    ['tituloMenosPlazas', 'Más concurrido de lo habitual'],
    ['tituloIgualPlazas', 'Ocupación habitual'],
  ]),
  ca_ES: new Map([
    ['leyendaPlazasLibres', 'Places lliures'],
    ['tituloMasPlazas', 'Menys concorregut del que és habitual'],
    ['tituloMenosPlazas', 'Més concorregut del que és habitual'],
    ['tituloIgualPlazas', 'Ocupació habitual'],
  ]),
};
export default TEXT_PUBLIC_PARKINGS;
