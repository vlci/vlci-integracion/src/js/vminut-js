import { getBaseURI } from '../utils/env-utils';

const insertValues = (row) => {
  const element = document.getElementById(`${row.categoria}-${row.grupo}`);
  if (element) {
    element.innerHTML = row.total;
  }
};
const loadSmartParkings = (data) => data.map(insertValues);

/*
 *  Funcion para obtener los datos de la query
 */
export default async function getDataSmartParkings() {
  try {
    const response = await fetch(`${getBaseURI()}smartparkings.cors`);
    const data = await response.json();
    loadSmartParkings(data);
  } catch (error) {
    console.log(error);
  }
}
