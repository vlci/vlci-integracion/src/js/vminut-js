export const hostNameFirstSplit = window.location.hostname.split('.')[0];
export const envUtil = {
  wwwint: {
    env: 'INT',
    isPro: false,
    geoportalHostname: 'https://geoportal.valencia.es',
    valenbisiPath: '/server/rest/services/OPENDATA/Trafico/MapServer/228/',
  },
  wwwpre: {
    env: 'PRE',
    isPro: false,
    geoportalHostname: 'https://geoportalpre.valencia.es',
    valenbisiPath: '/server/rest/services/Open_Data/Transporte/MapServer/228/',
  },
  www: {
    env: 'PRO',
    isPro: true,
    geoportalHostname: 'https://geoportal.valencia.es',
    valenbisiPath: '/server/rest/services/OPENDATA/Trafico/MapServer/228/',
  },
  wwwpro: {
    env: 'PRO',
    isPro: true,
    geoportalHostname: 'https://geoportal.valencia.es',
    valenbisiPath: '/server/rest/services/OPENDATA/Trafico/MapServer/228/',
  },
  getEnvironment: () => envUtil[hostNameFirstSplit].env,
  getGeoportalHostname: () => envUtil[hostNameFirstSplit].geoportalHostname,
  isProduction: () => envUtil[hostNameFirstSplit].isPro,
  getValenbisiPath: () => envUtil[hostNameFirstSplit].valenbisiPath,
  getValenbisiURL: () =>
    envUtil[hostNameFirstSplit].geoportalHostname + envUtil[hostNameFirstSplit].valenbisiPath,
};

/**
 * Returns de Base URI for the Backend API Calls
 * If the global variable: forcePRE or forcePROD exists and are set to true,
 * overwrites with the PRE or PROD URI
 * @returns
 */
export function getBaseURI() {
  let baseURI = 'https://www.valencia.es/web/valenciaalminut/';

  const { hostname } = window.location;
  if (hostname === 'wwwint.valencia.es') {
    baseURI = 'https://wwwint.valencia.es/web/guest/valencia-al-minut-city-dashboad/';
  } else if (hostname === 'wwwpre.valencia.es') {
    baseURI = 'https://wwwpre.valencia.es/web/guest/valencia-al-minut-city-dashboad/';
  } else if (hostname === 'www.valencia.es') {
    baseURI = 'https://www.valencia.es/web/guest/valenciaalminut/';
  }

  try {
    if (forcePRE) {
      baseURI = 'https://wwwpre.valencia.es/web/guest/valencia-al-minut-city-dashboad/';
    }
    if (forcePROD) {
      baseURI = 'https://wwwpre.valencia.es/web/guest/valenciaalminut/';
    }
  } catch (ex) {
    // Ignore errors if the variables are not defined
  }

  return baseURI;
}
