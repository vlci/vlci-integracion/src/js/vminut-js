import { envUtil } from '../utils/env-utils';

/* Sería muy interesante saber el nombre o descripción
 * de estas capas para hacer las urls más legibles.
 */
const layers = {
  contAtmAcusticaSensor9956: 'ContaminacionAtmosfericaAcusticaSensor_9956',
  medioAmb1654: 'MedioAmbiente_1654',
  medioAmb3493: 'MedioAmbiente_3493',
  medioAmb9063: 'MedioAmbiente_9063',
  medioAmb9853: 'MedioAmbiente_9853',
  ociSonometros1365: 'OCISonometros_1365',
  taxi461: 'PMTCyDTaxi_Externo_461',
  taxi461Sub0: 'PMTCyDTaxi_Externo_461_0',
  taxi461Sub1: 'PMTCyDTaxi_Externo_461_1',
  taxi461Sub3: 'PMTCyDTaxi_Externo_461_3',
  taxi461Sub4: 'PMTCyDTaxi_Externo_461_4',
  taxi461Sub6: 'PMTCyDTaxi_Externo_461_6',
  taxi461Sub7: 'PMTCyDTaxi_Externo_461_7',
  seguimientoCoches1965: 'Seguimiento_Coches_EMT_1965',
  seguimientoCoches1965Sub384: 'Seguimiento_Coches_EMT_1965_384',
  trafico8754: 'Trafico_8754',
  trafico9370: 'Trafico_9370',
  transporte2747: 'Transporte_2747',
  transporte3800: 'Transporte_3800',
  transporte493: 'Transporte_493',
  transporte6912: 'Transporte_6912',
  transporte6912Sub189: 'Transporte_6912_189',
  transporte6912Sub190: 'Transporte_6912_190',
  transporte6912Sub192: 'Transporte_6912_192',
  transporte6912Sub209: 'Transporte_6912_209',
  transporte7201: 'Transporte_7201',
  turismo7094: 'Turismo_7094'
};

const breakPoints = {
  breakPoint300: 'mobileBreakPoint=300',
};

const mapScales = {
  scale8000: 'scale=8000',
  scale8500: 'scale=8500',
  scale9500: 'scale=9500',
  scale15000: 'scale=15000',
  scale40000: 'scale=40000',
};

const mapCenter = {
  latitude: '-41823.8565',
  longitude: '4789704.9643',
  wkid: '102100',
};

export function loadIframe(id) {
  /* IMPORTANTE: El comentario de la siguiente linea (if) se ha realizado para posibilitar
   * la recarga del iframe cada vez que se de al botón de unos de los mapas.
   * Esto no es correcto tecnicamente ya que lo que nos interesa es que una vez cargado el mapa...
   * no tenga que volver a cargarse.
   * Por el momento se a prescindido de esta funcionalidad ya que cuanto
   * se cargan más de 2 mapas en chrome se empieza a perder la conexión con los anteriores.
   * Esto esta reportado.
   */
  const geoportalHostName = envUtil.getGeoportalHostname();
  const appValenciaAlMinut = '/apps/ValenciaAlMinut/?showLayers';
  const frameElement = document.getElementById(id);

  switch (id) {
    case 'iframe-emt':
      frameElement.src = `${geoportalHostName}${appValenciaAlMinut}=${layers.seguimientoCoches1965};${layers.seguimientoCoches1965Sub384};${layers.transporte7201};${layers.transporte2747};${layers.transporte3800};${layers.transporte_9565};&extent=-0.39,39.4661,-0.3647,39.4738&${mapScales.scale8000}&${breakPoints.breakPoint300};`;
      break;
    case 'iframe-transit':
      frameElement.src = `${geoportalHostName}${appValenciaAlMinut}=${layers.transporte6912};${layers.transporte6912Sub192};${layers.transporte6912Sub190};${layers.trafico8754};&${breakPoints.breakPoint300};`;
      break;
    case 'iframe-oratge':
      frameElement.src = `${geoportalHostName}${appValenciaAlMinut}=${layers.medioAmb9853};${layers.medioAmb3493};${layers.ociSonometros1365};${layers.contAtmAcusticaSensor9956};${layers.medioAmb9063};&extent=-0.4295,39.454,-0.3284,39.485&${mapScales.scale9500}&${breakPoints.breakPoint300};`;
      break;
    case 'iframe-talls-carrers':
      frameElement.src = `${geoportalHostName}${appValenciaAlMinut}=${layers.transporte6912};${layers.transporte6912Sub209}&center=${mapCenter.latitude}%2C${mapCenter.longitude}%2C${mapCenter.wkid}&${mapScales.scale15000}&${breakPoints.breakPoint300};`;
      break;
    case 'iframe-ecoparcs':
      frameElement.src = `${geoportalHostName}${appValenciaAlMinut}=${layers.medioAmb1654}&center=${mapCenter.latitude}%2C${mapCenter.longitude}%2C${mapCenter.wkid}&${mapScales.scale15000}&${breakPoints.breakPoint300};`;
      break;
    case 'iframe-valenbisi':
      frameElement.src = `${geoportalHostName}${appValenciaAlMinut}=${layers.transporte6912};${layers.transporte6912Sub189};${layers.transporte493};&center=${mapCenter.latitude}%2C${mapCenter.longitude}%2C${mapCenter.wkid}&${mapScales.scale8500}&${breakPoints.breakPoint300};`;
      break;
    case 'iframe-wifi':
      frameElement.src = `${geoportalHostName}${appValenciaAlMinut}=${layers.turismo7094}&center=${mapCenter.latitude}%2C${mapCenter.longitude}%2C${mapCenter.wkid}&${mapScales.scale40000}&${breakPoints.breakPoint300};`;
      break;
    case 'iframe-parquing':
      frameElement.src = `${geoportalHostName}${appValenciaAlMinut}=${layers.taxi461};${layers.taxi461Sub0};${layers.taxi461Sub1};${layers.taxi461Sub3};${layers.taxi461Sub4};${layers.taxi461Sub6};${layers.taxi461Sub7};${layers.trafico9370}&center=${mapCenter.latitude}%2C${mapCenter.longitude}%2C${mapCenter.wkid}&${mapScales.scale8000}&${breakPoints.breakPoint300};`;
      break;
    default:
      break;
  }
}

export function changeIframe(event) {
  const id = event.target.getAttribute('data-iframe');
  loadIframe(id);
}
