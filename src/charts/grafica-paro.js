import { init } from 'echarts';
import { tooltipFormat } from './common-graficas';
import TEXT_PARO from '../paro/textos';

const IDIOMA = Liferay.ThemeDisplay.getLanguageId();

export default function graficaDatosParo(data, dataTotal) {
  /*
   * La variable arrayDatosParo recibe valor en un formulario de liferay
   */

  const myChart = init(document.getElementById('myChartParo'));
  // const namesRow = arrayDatosParo[0];
  // const dataRows = arrayDatosParo.slice(1);
  const meses = [];
  const paroMujeres = [];
  const paroHombres = [];
  const paroTotal = dataTotal.map((element) => element.kpivalue);

  data.forEach((element, index) => {
    if (index % 2 === 0) {
      meses.push(
        `${TEXT_PARO[IDIOMA].get(
          element.calculationperiod.substring(5),
        )} ${element.calculationperiod.substring(0, 4)}`,
      );
    }

    if (element.sliceanddicevalue === TEXT_PARO[IDIOMA].get('GeneroM')) {
      paroHombres.push(parseInt(element.kpivalue, 10));
    } else {
      paroMujeres.push(parseInt(element.kpivalue, 10));
    }
  });

  // Los límites para los ejes de las gráficas
  const maxBarChart = Math.max(...paroHombres.concat(paroMujeres)) * 2;
  const minLineChart = Math.min(...paroTotal) / 1.2;
  const maxLineChart = Math.max(...paroTotal);

  const option = {
    tooltip: {
      trigger: 'item',
      show: true,
      textStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
      },
      formatter: tooltipFormat,
    },
    grid: {
      top: '15%',
      left: '3%',
      right: '3%',
      bottom: '3%',
    },
    xAxis: [
      {
        type: 'category',
        data: meses,
        show: false,
      },
    ],
    yAxis: [
      {
        type: 'value',
        max: maxBarChart,
        min: 0,
        show: false,
      },
      {
        type: 'value',
        max: maxLineChart,
        min: minLineChart,
        show: false,
      },
    ],
    series: [
      {
        name: `${TEXT_PARO[IDIOMA].get('Paro')} ${TEXT_PARO[IDIOMA].get('Total')}`,
        type: 'line',
        smooth: true,
        yAxisIndex: 1,
        areaStyle: {
          opacity: 0.5,
        },
        symbol: 'dot',
        tooltip: {
          position: 'bottom',
          confine: true,
        },
        data: paroTotal,
        zlevel: 1,
        color: '#FFCD00',
      },
      {
        name: `${TEXT_PARO[IDIOMA].get('Paro')} ${TEXT_PARO[IDIOMA].get('GeneroF')}`,
        type: 'bar',
        data: paroMujeres,
        zlevel: 2,
        color: '#AB91C9',
      },
      {
        name: `${TEXT_PARO[IDIOMA].get('Paro')} ${TEXT_PARO[IDIOMA].get('GeneroM')}`,
        type: 'bar',
        data: paroHombres,
        zlevel: 2,
        color: '#8E8E8D',
      },
    ],
  };
  myChart.setOption(option);
}
