import { init } from 'echarts';

/*
 * Esta función asignar un color de un array de colores dado un valor y un valor máximo
 * @ param valor Valor de la gráfica
 * @ param valorMax Valor máximo de la gráfica
 * @ return color Color que se asigna a la gráfica
 */
function calcularColor(valor, valorMax) {
  const arrayColores = [
    '#EEF59A',
    '#EEF59A',
    '#E0EE8C',
    '#D2E67D',
    '#C4DF6F',
    '#B6D761',
    '#A8D052',
    '#9AC844',
    '#8CC136',
    '#7EB927',
    '#70B219',
  ];

  const valorSerie = arrayColores.length;
  const valorParte = valorMax / valorSerie;

  const colorIndex = Math.floor(valor / valorParte);
  return arrayColores[Math.min(colorIndex, valorSerie - 1)];
}

/*
 * Esta función dibuja una gráfica gauge en un html
 * @ DOMElementId Id del componente HTML
 * @ param valor Valor de la gráfica
 * @ param valorMax Valor máximo de la gráfica
 */
export default function dibujarGauge(DOMElementId, valor, valorMax) {
  const myChart = init(document.getElementById(DOMElementId));
  const colorGraf = calcularColor(valor, valorMax);

  let option = {};

  option = {
    series: [
      {
        type: 'gauge',
        center: ['50%', '60%'],
        startAngle: 200,
        endAngle: -20,
        min: 0,
        max: valorMax,
        splitNumber: 0,
        progress: {
          show: true,
          width: 5,
          itemStyle: {
            color: '#666',
          },
        },
        pointer: {
          show: false,
        },
        axisLine: {
          lineStyle: {
            width: 5,
            color: [
              [0, '#EEF59A'],
              [0.1, '#EEF59A'],
              [0.2, '#E0EE8C'],
              [0.3, '#D2E67D'],
              [0.4, '#C4DF6F'],
              [0.5, '#B6D761'],
              [0.6, '#A8D052'],
              [0.7, '#9AC844'],
              [0.8, '#8CC136'],
              [0.9, '#7EB927'],
              [1, '#70B219'],
            ],
          },
        },
        axisTick: {
          show: false,
        },
        splitLine: {
          show: false,
        },
        axisLabel: {
          show: false,
        },
        title: {
          offsetCenter: [0, '-20%'],
          fontSize: 30,
        },
        detail: {
          fontSize: 10,
          offsetCenter: [0, '0%'],
          valueAnimation: true,
          color: '#333',
        },
        data: [
          {
            value: valor,
          },
        ],
      },
      {
        type: 'gauge',
        center: ['50%', '60%'],
        startAngle: 200,
        endAngle: -20,
        min: 0,
        max: valorMax,
        itemStyle: {
          color: colorGraf,
        },
        progress: {
          show: true,
          width: 10,
        },
        pointer: {
          show: false,
        },
        axisLine: {
          show: false,
        },
        axisTick: {
          show: false,
        },
        splitLine: {
          show: false,
        },
        axisLabel: {
          show: false,
        },
        detail: {
          show: false,
        },
        data: [
          {
            value: valor,
          },
        ],
      },
    ],
  };
  // Pintamos el grafico pasando las opciones
  myChart.setOption(option);
}
