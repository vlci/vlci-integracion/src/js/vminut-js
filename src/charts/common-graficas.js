/**
 * Función que cambia la location de un numero al idioma es-ES (Separador de miles y decimales).
 * @param {String} value Valor o numero expresado como string.
 * @returns Devuelve el mismo valor adaptado al idioma es-ES.
 */
export const valueToLocaleString = (value) => value.toLocaleString('es-ES');
export const DIAS_DATAZOOM = 60;

/**
 * Función que devuelve el formato para el toolkip de las graficas
 * @param {Object} params Objeto proporcionado por la libreria echarts con
 * todos los parametros que posee un tooltip.
 * @returns Devuelve un string html con el formato que se mostrara en el tooltip de la página.
 */
export function tooltipFormat(params) {
  const fecha = Liferay.ThemeDisplay.getBCP47LanguageId() === 'es-ES' ? 'Fecha' : 'Data';
  const value = valueToLocaleString(params.value);
  const html = `
  <div style="font-weight: bold; font-size:11px;">
    <span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:${params.color}"></span>
    ${fecha}: ${params.name}
  </div>
  <div>
    ${params.seriesName} : <span style="font-weight: bold">${value}</span>
  </div>
   
  `;
  return html;
}

/**
 * Función que formatea una fecha dd/MM/yyyy en dd/MM
 * @param {fechas} params valores obtenidos de base de datos en formato dd/MM/yyyy
 * @returns Devuelve la fecha formateada en dd/mm
 */
export function formatDateMonthDay(fechas) {
  return fechas.map((element) => element.slice(0, 5));
}

/**
 * Función que devuelve la fechas en el formato correcto según la gráfica que estemos mostrando
 * @param {GraficasData} params array que contiene las propiedades de las gráficas
 * @param {fechas} params valores obtenidos de base de datos en formato dd/MM/yyyy
 * @param {index} params indice del array de gráficas
 * @returns Devuelve la fecha formateada
 */
export function setDates(GraficasData, fechas, index) {
  return GraficasData[index].DOMElementId === 'tempreatura_data'
    ? formatDateMonthDay(fechas)
    : fechas;
}

export function obtenerValorInicioDataZoom(grafica) {
  return grafica.DOMElementId === 'tempreatura_data'
    ? 0
    : grafica.data[0].data.length - DIAS_DATAZOOM;
}

export function getCommonOptions(grafica, startZoom) {
  return {
    tooltip: {
      trigger: 'item',
      show: true,
      textStyle: {
        fontSize: 10,
        fontFamily: 'Montserrat',
      },
      formatter: tooltipFormat,
    },
    legend: {
      type: 'plain',
      data: grafica.series.sort(),
      top: 15,
      textStyle: {
        fontSize: 11,
        fontFamily: 'Montserrat',
        fontWeight: '600',
      },
    },
    grid: {
      left: '3%',
      right: '3%',
      bottom: '15%',
      containLabel: true,
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: grafica.fechas,
      splitLine: {
        show: false,
      },
      axisLabel: {
        textStyle: {
          fontSize: 9,
          fontFamily: 'Montserrat',
          color: '#000',
          fontWeight: 'bolder',
        },
        margin: 15,
      },
    },
    yAxis: {
      name: grafica.yAxisName,
      nameLocation: 'center',
      nameGap: grafica.nameGap,
      nameTextStyle: {
        fontWeight: 'normal',
      },
      type: 'value',
      scale: true,
      axisLabel: {
        textStyle: {
          fontSize: 9,
          fontFamily: 'Montserrat',
          color: '#000',
          fontWeight: 'bolder',
        },
        formatter: valueToLocaleString,
      },
    },
    series: grafica.data,
    dataZoom: [
      {
        show: 0,
        bottom: 5,
        startValue: startZoom,
        borderColor: '#ddd',
        fillerColor: 'rgba(0,0,0,0)',
        textStyle: {
          fontSize: 10,
          fontFamily: 'Montserrat',
          color: '#000',
          fontWeight: 'bolder',
          textBorderColor: '#FFF',
          textBorderWidth: 3,
        },
        selectedDataBackground: {
          lineStyle: {
            color: '#333',
          },
          areaStyle: {
            color: '#ffcd00',
          },
        },
        dataBackground: {
          backgroundColor: 'rgba(0,0,0,1)',
          lineStyle: {
            color: '#333',
          },
          areaStyle: {
            color: 'rgba(0,0,0,0.5)',
          },
        },
        handleStyle: {
          color: 'rgba(0,0,0,0.2)',
        },
        moveHandleStyle: {
          color: 'rgba(0,0,0,0.3)',
        },
        emphasis: {
          handleStyle: {
            color: 'rgba(255,205,0,1)',
          },
          moveHandleStyle: {
            color: 'rgba(255,205,0,1)',
          },
        },
      },
    ],
  };
}

export function getDataOrderByName(data, dataNext) {
  const name = data.name.toUpperCase();
  const nameNext = dataNext.name.toUpperCase();

  if (name < nameNext) {
    return -1; // Si el nombre A es menor que el nombre B, colocar A antes de B
  }

  if (name > nameNext) {
    return 1; // Si el nombre A es mayor que el nombre B, colocar A después de B
  }

  return 0; // Si los nombres son iguales, mantener el orden original
}
