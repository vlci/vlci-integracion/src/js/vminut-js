import {
  configureLinesChartValidates,
  configureDataValidate,
  resultsetWithFields,
  configureDataLine,
  calculateXAxisStart
} from 'vlcishared';
import { init } from 'echarts';
import { getBaseURI } from '../utils/env-utils';
import {
  getCommonOptions,
  setDates,
  obtenerValorInicioDataZoom,
  valueToLocaleString
} from './common-graficas';

// OBTENCION Y PREPARACION DE DATOS
// Declaración
const DIAS_DATAZOOM_MOBILE = 30;

function loadEchartsScript(callback) {
  const script = document.createElement('script');
  script.src = 'https://cdn.jsdelivr.net/npm/echarts/dist/echarts.min.js';
  script.onload = () => {
    console.log('echarts cargado correctamente');
    callback();
  };
  script.onerror = () => {
    console.error('Error al cargar el script de echarts');
  };
  document.head.appendChild(script);
}

/**
 * Función para obtener el nombre de las series en el idioma actual de la pagina
 * @param {Array} labels Array de objetos que contiene descripciones
 * de las series de la grafica en distintos idiomas.
 * @returns Devuelve el nombre de las series en el idioma actual de la pagina.
 */
const seriesMap = (labels) =>
  labels.map((e) => e[Liferay.ThemeDisplay.getLanguageId()] || e.default);
const language = Liferay.ThemeDisplay.getLanguageId();
const limit2anyos = 730;

// Objeto contenedor de los datos de las graficas
const GraficasData = {
  motorizado: {
    url: `${getBaseURI()}vehiculos.cors?limit=${limit2anyos}&idioma=${language}`,
    DOMElementId: 'grafica_coches_data',
    nameGap: 55,
    nameGapMobile: 37,
    yAxisName: '',
    fechas: [],
    series: [],
    data: [],
  },
  anillociclista: {
    url: `${getBaseURI()}bicis.cors?limit=${limit2anyos}&idioma=${language}`,
    DOMElementId: 'grafica_bicis_data',
    nameGap: 45,
    nameGapMobile: 28,
    yAxisName: '',
    fechas: [],
    series: [],
    data: [],
  },
  agua: {
    url: `${getBaseURI()}agua.cors?limit=${limit2anyos}&idioma=${language}`,
    DOMElementId: 'consumo_agua',
    nameGap: 50,
    nameGapMobile: 39,
    yAxisName: '',
    seriesLabel: seriesMap([
      {
        es_ES: 'Inyección Total de Agua',
        ca_ES: "Injecció Total d'aigua",
        default: 'Inyección Total de Agua',
      },
    ]),
    fechas: [],
    series: [],
    data: [],
  },
  emttotal: {
    url: `${getBaseURI()}emttotal.cors?limit=${limit2anyos}&idioma=${language}`,
    DOMElementId: 'grafica_emt',
    nameGap: 55,
    nameGapMobile: 42,
    yAxisName: '',
    seriesLabel: seriesMap([
      {
        es_ES: 'Número de Viajes EMT',
        ca_ES: 'Nombre viatges EMT',
        default: 'Número de Viajes EMT',
      },
    ]),
    fechas: [],
    series: [],
    data: [],
  },
  residuos: {
    url: `${getBaseURI()}residuos.cors?limit=${limit2anyos}`,
    DOMElementId: 'kgs_residuos_data',
    nameGap: 55,
    nameGapMobile: 45,
    yAxisName: '',
    fechas: [],
    series: [],
    data: [],
  },
  calidadaireNO2: {
    url: `${getBaseURI()}calidadaireNO2.cors?limit=${limit2anyos}`,
    DOMElementId: 'grafica_no2_data',
    nameGap: 30,
    nameGapMobile: 19,
    yAxisName: '',
    fechas: [],
    series: [],
    data: [],
  },
  temperaturaMedia: {
    url: `${getBaseURI()}temperaturaMedia.cors`,
    DOMElementId: 'tempreatura_data',
    nameGap: 25,
    nameGapMobile: 16,
    yAxisName: '',
    renameYAxisName(serieIndex, serieName, index) {
      const languageId = Liferay.ThemeDisplay.getLanguageId();
      if (serieName.includes('Media diaria desde 2015') && languageId === 'ca_ES') {
        const valorVal = serieName
          .replace('Media diaria desde', 'Mitjana diària des de')
          .replace('hasta', 'fins a');
        GraficasData[index].series.push(valorVal);
        GraficasData[index].data[serieIndex].name = valorVal;
      } else {
        GraficasData[index].series.push(serieName);
      }
    },
    fechas: [],
    series: [],
    data: [],
  },
};
const setDataFromResponseBody = (responseBody, index) => {
  const { series } = responseBody;

  if (Object.keys(series).length > 0) {
    GraficasData[index].yAxisName = responseBody.measureUnit;

    GraficasData[index].fechas = setDates(GraficasData, responseBody.fechas, index);

    let serieIndex = -1;
    Object.keys(series).forEach((serieName) => {
      // Aumentamos el indice
      serieIndex += 1;

      // Construimos el objeto data
      const dataObject = {
        name: serieName,
        type: 'line',
        symbol: 'circle',
        symbolSize: 6,
        data: [],
      };
      dataObject.data = responseBody.series[serieName].data;
      GraficasData[index].data.push(dataObject);

      /* Si la gráfica llama a la función renameYAxisName, significa que hay nombres que
       * obtendrá de base de datos y otros que deben ser sustituidos
       * Si la grafica tiene el atributo seriesLabel, cogeremos los nombres de
       * las series indicados en el y lo sustituiremos en el array Data tambien.
       * IMPORTANTE: El orden de los nombres definidos en el seriesLabel debe coincidir
       * con el orden de los datos de BBDD.
       * Si la grafica no tiene el atributo seriesLabel, el nombre de las series se cogerá de BBDD.
       */
      if (GraficasData[index].renameYAxisName) {
        GraficasData[index].renameYAxisName(serieIndex, serieName, index);
      } else if (GraficasData[index].seriesLabel) {
        GraficasData[index].series.push(GraficasData[index].seriesLabel[serieIndex]);
        GraficasData[index].data[serieIndex].name = GraficasData[index].seriesLabel[serieIndex];
      } else {
        GraficasData[index].series.push(serieName);
      }
    });
  }
};

async function getDataGrafica(url) {
  let data = {};
  try {
    const response = await fetch(`${url}`);
    data = await response.json();
  } catch (error) {
    console.log(error);
  }
  return data;
}

function dibujarGrafica(tipo) {
  const grafica = GraficasData[tipo];
  const myChart = init(document.getElementById(grafica.DOMElementId));
  const startZoom = obtenerValorInicioDataZoom(grafica);
  const option = getCommonOptions(grafica, startZoom);

  // Cambiamos propiedades del options si estamos en un dispositivo movil.
  if (Liferay.Browser.isMobile()) {
    option.yAxis.nameGap = grafica.nameGapMobile;
    option.yAxis.nameTextStyle = {
      fontSize: 9,
    };
    option.dataZoom[0].startValue = grafica.data[0].data.length - DIAS_DATAZOOM_MOBILE;
  }

  // Pintamos el grafico pasando las opciones
  myChart.setOption(option);
}

function dibujarGraficaValidados(datos, kpiValidList, kpiInvalidList, grafica) {
  const datosArray = datos.resultset || datos;
  // Obtenemos los datos ordenados
  const cleanData = configureDataLine(datosArray);
  const date = cleanData[1];
  const xAxisData = Array.from(date.values());
  const legendData = cleanData[2];

  // Obtenemos los datos para formar la serie
  const resultSet = resultsetWithFields(datos);
  const seriesData = configureDataValidate(
    resultSet,
    cleanData[0],
    kpiValidList,
    kpiInvalidList,
    language
  );
  const dataZoomStart = calculateXAxisStart(date.size);
  const title = (language === "ca_ES")? resultSet[0].measureunit_ca : resultSet[0].measureunit_es;

  const specificOptions = {
    xAxis: {
      data: xAxisData,
      axisLabel: {
        textStyle: {
          fontSize: 9,
        },
        margin: 15,
      }
    },
    yAxis: {
      name: title,
      nameLocation: 'center',
      nameGap: grafica.nameGap,
      axisLabel: {
        textStyle: {
          fontSize: 9,
        },
        formatter: valueToLocaleString,
      },
    },
    grid: {
      left: '3%',
      right: '3%',
      bottom: '15%',
      top: '20%'
    },
    legend: {
      show: true,
      data: legendData,
      top: 10,
      textStyle: {
        fontSize: 11,
        fontWeight: '600',
      }
    },
    dataZoom: {
      show: true,
      data: xAxisData,
      start: dataZoomStart
    },
    toolbox: {
      show: true
    },
  };
  configureLinesChartValidates(
    grafica.DOMElementId,
    seriesData,
    specificOptions
  );
}

// Funcion para obtener los datos de las graficas y pintarlas
export default function graficas() {
  loadEchartsScript(() => {
    getDataGrafica(GraficasData.motorizado.url).then((datos) => {
      const kpiValidList = ['KpiValid-30-Accesos-Ciudad-Coches', 'KpiValid-365-Accesos-Ciudad-Coches'];
      const kpiInvalidList = ['Kpi-Accesos-Ciudad-Coches'];
      dibujarGraficaValidados(datos, kpiValidList, kpiInvalidList, GraficasData.motorizado);
    });
    getDataGrafica(GraficasData.anillociclista.url).then((datos) => {
      const kpiValidList = ['KpiValid-365-Trafico-Bicicletas-Anillo', 'KpiValid-30-Trafico-Bicicletas-Anillo'];
      const kpiInvalidList = ['Kpi-Trafico-Bicicletas-Anillo'];
      dibujarGraficaValidados(datos, kpiValidList, kpiInvalidList, GraficasData.anillociclista);
    });
    getDataGrafica(GraficasData.agua.url, 'agua').then((data) => {
      setDataFromResponseBody(data, 'agua');
      dibujarGrafica('agua');
    });
    getDataGrafica(GraficasData.emttotal.url, 'emttotal').then((data) => {
      setDataFromResponseBody(data, 'emttotal');
      dibujarGrafica('emttotal');
    });
    getDataGrafica(GraficasData.residuos.url, 'residuos').then((data) => {
      setDataFromResponseBody(data, 'residuos');
      dibujarGrafica('residuos');
    });
    getDataGrafica(GraficasData.calidadaireNO2.url, 'calidadaireNO2').then((datos) => {
      const kpiValidList = [
        'A01_AVFRANCIA_24h_va',
        'A02_BULEVARDSUD_24h_vm',
        'A07_VALENCIACENTRE_24h_va',
        'A07_VALENCIACENTRE_24h_vm',
        'A05_POLITECNIC_24h_vm',
        'A01_AVFRANCIA_24h_vm',
        'A03_MOLISOL_24h_va',
        'A06_VIVERS_24h_vm',
        'A03_MOLISOL_24h_vm',
        'A06_VIVERS_24h_va',
        'A05_POLITECNIC_24h_va',
        'A04_PISTASILLA_24h_vm',
        'A02_BULEVARDSUD_24h_va',
        'A04_PISTASILLA_24h_va'
      ];
      const kpiInvalidList = [
        'A05_POLITECNIC_24h',
        'A03_MOLISOL_24h',
        'A01_AVFRANCIA_24h',
        'A06_VIVERS_24h',
        'A04_PISTASILLA_24h',
        'A07_VALENCIACENTRE_24h',
        'A02_BULEVARDSUD_24h'
      ]
      dibujarGraficaValidados(datos, kpiValidList, kpiInvalidList, GraficasData.calidadaireNO2);
    });
    getDataGrafica(GraficasData.temperaturaMedia.url, 'temperaturaMedia').then((data) => {
      setDataFromResponseBody(data, 'temperaturaMedia');
      dibujarGrafica('temperaturaMedia');
    });
  });
}
