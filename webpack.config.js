const path = require('path');
const webpack = require('webpack');

module.exports = {
  mode: 'production',
  entry: { legacy: './src/valenciaalminut.js' },
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    filename: '[name].js',
    library: '[name]',
    libraryTarget: 'umd',
    clean: true,
  },
  optimization: {
    usedExports: true,
  },
  plugins: [
    // fix "process is not defined" error:
    // (do "npm install process" before running the build)
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
  ],
  resolve: {
    alias: {
      vlcishared: path.resolve(__dirname, 'node_modules/@vlci/vlcishared/dist/js/vlcishared.js'),
    },
  },
};
